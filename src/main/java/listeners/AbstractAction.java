package listeners;

import layout.Controlls;
import logic.Hints;
import logic.Logic;

import java.awt.event.ActionListener;

public abstract class AbstractAction implements ActionListener {

    protected Controlls controlls;

    public AbstractAction(Controlls controlls) {
        this.controlls = controlls;
    }

    protected void initValues(boolean isGender) {
        String[] randomElement = Logic.getInstance().getRandomElement(isGender);
        controlls.getWordLabel().setText(randomElement[0]);
        Hints.setHint(Hints.getAllAsterisks(randomElement[1]));
        controlls.getResultField().setText("");
        Hints.unCheckAllHints();
        Hints.setEnableAllHints(false);
        Hints.setEnableFirstHint(true);
        controlls.getCheckButton().setEnabled(true);
        controlls.getShowResultButton().setEnabled(true);
    }
}
