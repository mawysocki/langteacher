package listeners;

import layout.Controlls;
import logic.Hints;
import logic.Logic;
import logic.Points;

import java.awt.event.ActionEvent;

public class CheckListener extends AbstractAction {

    public CheckListener(Controlls controlls) {
        super(controlls);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        boolean isCorrect = Logic.getInstance().isCorrect(controlls.getResultField().getText());
        System.out.println(isCorrect);
        if (isCorrect) {
            Points.addPoints(Logic.getInstance().getCurrentValues()[1]);
            Logic.getInstance().removeCurrentItem();
            if (!Logic.getInstance().isEmpty()) {
                initValues(false);
            }
            else {
                Points.displayPoints();
                controlls.setEnableAllButtons(false);
                Hints.setEnableAllHints(false);
            }
        }
    }
}
