package listeners;

import connectors.ExcelManager;
import layout.Controlls;
import logic.Hints;
import logic.Logic;
import logic.Points;

import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;

public class TopicChangeListener implements ItemListener {
    Controlls controlls;
    public TopicChangeListener(Controlls controlls) {
        this.controlls = controlls;
    }

    @Override
    public void itemStateChanged(ItemEvent e) {
        if (e.getStateChange() == ItemEvent.SELECTED) {
            Object item = e.getItem();
            boolean isGender = false;
            new ExcelManager(item.toString(), 1, isGender);
            setInitialValue(isGender);
            enableFields();
        }
    }

    private void setInitialValue(boolean isGender) {
        String[] randomElement = Logic.getInstance().getRandomElement(isGender);
        controlls.getWordLabel().setText(randomElement[0]);
        Hints.setHint(Hints.getAllAsterisks(randomElement[1]));
        controlls.getResultField().setText("");
        Hints.unCheckAllHints();
    }

    private void enableFields() {
        controlls.setEnableAllButtons(true);
        controlls.getResultField().setEnabled(true);
        Hints.setEnableAllHints(false);
        Hints.setEnableFirstHint(true);
    }


}
