package listeners;

import layout.Controlls;
import logic.Hints;
import logic.Logic;

import java.awt.event.ActionEvent;

public class ShowListener extends AbstractAction {
    public ShowListener(Controlls controlls) {
        super(controlls);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        Hints.setEnableAllHints(false);
        controlls.getCheckButton().setEnabled(false);
        controlls.getShowResultButton().setEnabled(false);
        controlls.getHintLabel().setText(Logic.getInstance().getCurrentValues()[1]);

    }
}
