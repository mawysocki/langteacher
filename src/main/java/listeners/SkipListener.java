package listeners;

import layout.Controlls;

import java.awt.event.ActionEvent;

public class SkipListener extends AbstractAction{

    public SkipListener(Controlls controlls) {
        super(controlls);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        initValues(false);
    }
}
