package window;

import connectors.ExcelManager;
import layout.CenterPane;
import layout.Controlls;
import layout.LeftWingPane;
import layout.RightWingPane;
import logic.Hints;

import javax.swing.*;
import java.awt.*;

public class MainWindow extends JFrame {
    public static final Dimension SCREEN_SIZE = Toolkit.getDefaultToolkit().getScreenSize();
    public final Dimension WINDOW_SIZE = new Dimension(1000, 600);
    Controlls controlls;
    ExcelManager mng;
    public MainWindow() {
        setupFrame();
        controlls = new Controlls();
        Hints.initControls(controlls);
        createPanels(controlls);
        this.setVisible(true);
    }

    private void setupFrame() {
        this.setLayout(new GridLayout(1, 3));
        this.setBounds(getFrameBounds());
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setResizable(false);

    }

    private Rectangle getFrameBounds() {
        int x = SCREEN_SIZE.width/2 - WINDOW_SIZE.width/2;
        int y = SCREEN_SIZE.height/2 - WINDOW_SIZE.height/2;
        int width = WINDOW_SIZE.width;
        int height = WINDOW_SIZE.height;
        return new Rectangle(x, y, width, height);
    }

    private void createPanels(Controlls controlls) {
        this.add(new LeftWingPane(controlls));
        this.add(new CenterPane(controlls));
        this.add(new RightWingPane(controlls));
    }
}
