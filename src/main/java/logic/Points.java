package logic;

import java.util.List;

public class Points {

    private static int maxPoints;
    private static int current;

    public static void addPoints(String word) {
        current+= word.length();
    }

    public static void calculateMaxPoints(List<String> dict) {
        for (String s : dict) {
            maxPoints += s.length();
        }
    }

    public static void displayPoints() {
        System.out.println(current + "/" + maxPoints);
    }

    public static void resetPoints() {
        current = 0;
    }
}
