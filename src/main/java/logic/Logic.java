package logic;

import java.util.List;
import java.util.Random;

public class Logic {

    private static Logic logic = new Logic();
    private List<String> toTranslateWords;
    private List<String> translations;
    private List<String> articles;

    private String[] currentValues;

    private int remainWords;

    private Logic() {
    }

    public static Logic getInstance() {
        return logic;
    }

    public void createDicts(List<String> espWords, List<String> polWords) {
        createDicts(espWords, polWords, null);
    }

    public void createDicts(List<String> espWords, List<String> polWords, List<String> articles) {
        this.toTranslateWords = espWords;
        this.translations = polWords;
        if (articles != null) {
            this.articles = articles;
        }
    }

    public String[] getRandomElement(boolean isGender) {
        currentValues = new String[3];
        Random r = new Random();
        remainWords = toTranslateWords.size();
        int index = r.nextInt(remainWords);
        currentValues[0] = toTranslateWords.get(index);
        currentValues[1] = translations.get(index);
        if (isGender) {
            currentValues[2] = articles.get(index);
        }
        return currentValues;
    }

    public boolean isCorrect(String proposal) {
        return currentValues[1].equalsIgnoreCase(proposal);
    }

    public String[] getCurrentValues() {
        return currentValues;
    }
    public void removeCurrentItem() {
        boolean isGender = false;
        System.out.println(toTranslateWords.size());
        toTranslateWords.remove(currentValues[0]);
        translations.remove(currentValues[1]);
        if (isGender) {
            articles.remove(currentValues[1]);
        }
        remainWords = toTranslateWords.size();
        System.out.println(toTranslateWords.size());
    }

    public boolean isEmpty() {
        return remainWords == 0;
    }
    public int getRemainWords() {
        return remainWords;
    }
}
