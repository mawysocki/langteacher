package logic;

import layout.Controlls;

public class Hints {
    private static Controlls controlls;

    public static void initControls(Controlls c) {
        controlls = c;
    }

    public static String getAllAsterisks(String word) {
        StringBuilder asterisks = new StringBuilder();
        for (int i = 0; i < word.length(); i++) {
            asterisks.append("*");
        }
        return asterisks.toString();
    }

    public static boolean isHintAvailable(String word, int hintLevel) {
        return word.length() / 2 >= hintLevel;
    }
    public static void setHint(String hint) {
        controlls.getHintLabel().setText(hint);
    }
    public static void setEnableFirstHint(boolean isEnabled) {
        controlls.getHint1().setEnabled(isEnabled);
    }
    public static void setEnableSecondHint(boolean isEnabled) {
        controlls.getHint2().setEnabled(isEnabled);
        if (isEnabled) {
            setEnableFirstHint(false);
        }
    }
    public static void setEnableThirdHint(boolean isEnabled) {
        controlls.getHint3().setEnabled(isEnabled);
        if (isEnabled) {
            setEnableSecondHint(false);
        }
    }
    public static void setEnableAllHints(boolean isEnabled) {
        setEnableFirstHint(isEnabled);
        setEnableSecondHint(isEnabled);
        setEnableThirdHint(isEnabled);
    }
    public static void unCheckAllHints() {
        controlls.getHint1().setSelected(false);
        controlls.getHint2().setSelected(false);
        controlls.getHint3().setSelected(false);
    }
}
