package layout;

import listeners.CheckListener;
import listeners.ShowListener;
import listeners.SkipListener;

import javax.swing.*;

public class RightWingPane extends AbstractPane{

    public RightWingPane(Controlls controlls) {
       super(controlls);
    }

    @Override
    public void initComponent() {
        controlls.getCheckButton().setText("Check");
        controlls.getCheckButton().addActionListener(new CheckListener(controlls));
        controlls.getShowResultButton().setText("Show");
        controlls.getShowResultButton().addActionListener(new ShowListener(controlls));
        controlls.getSkipButton().setText("Skip");
        controlls.getSkipButton().addActionListener(new SkipListener(controlls));
    }

    @Override
    public void setFontComponent() {
        controlls.getCheckButton().setFont(font);
        controlls.getShowResultButton().setFont(font);
        controlls.getSkipButton().setFont(font);
    }

    @Override
    public void addCompoment() {
        this.add(new JLabel());
        this.add(controlls.getCheckButton());
        this.add(controlls.getShowResultButton());
        this.add(controlls.getSkipButton());

    }

    @Override
    public void disableComponent() {
        controlls.setEnableAllButtons(false);
    }
}
