package layout;

import logic.Hints;

public class LeftWingPane extends AbstractPane {

    public LeftWingPane(Controlls controlls) {
        super(controlls);
    }

    @Override
    public void initComponent() {
        controlls.getWordLabel().setText("Word");
        controlls.getHint1().setText("Hint 1");
        controlls.getHint2().setText("Hint 2");
        controlls.getHint3().setText("Hint 3");
    }

    @Override
    public void setFontComponent() {
        controlls.getWordLabel().setFont(font);
        controlls.getHint1().setFont(font);
        controlls.getHint2().setFont(font);
        controlls.getHint3().setFont(font);
    }

    @Override
    public void addCompoment() {
        this.add(controlls.getWordLabel());
        this.add(controlls.getHint1());
        this.add(controlls.getHint2());
        this.add(controlls.getHint3());
    }

    @Override
    public void disableComponent() {
        Hints.setEnableAllHints(false);
    }
}
