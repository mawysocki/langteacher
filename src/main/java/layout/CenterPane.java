package layout;

import listeners.TopicChangeListener;
import connectors.ExcelConnector;

import javax.swing.*;

public class CenterPane extends AbstractPane{


    public CenterPane(Controlls controlls) {
        super(controlls);
    }

    @Override
    public void initComponent() {
        controlls.getResultField().setText("Result");
        controlls.getHintLabel().setText("Hint");
        ExcelConnector.listFilesUsingFilesList().forEach(controlls.getTopics()::addItem);
        controlls.getTopics().addItemListener(new TopicChangeListener(controlls));
        controlls.getTopics().setSelectedIndex(-1);
    }

    @Override
    public void setFontComponent() {
        controlls.getResultField().setFont(font);
        controlls.getHintLabel().setFont(font);
        controlls.getTopics().setFont(font);
    }

    @Override
    public void addCompoment() {
        this.add(controlls.getResultField());
        this.add(controlls.getHintLabel());
        this.add(new JLabel());
        this.add(controlls.getTopics());
    }

    @Override
    public void disableComponent() {
        controlls.getResultField().setEnabled(false);
    }
}
