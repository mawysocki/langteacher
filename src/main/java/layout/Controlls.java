package layout;

import javax.swing.*;

public class Controlls {

    private final JLabel wordLabel;
    private final JCheckBox hint1;
    private final JCheckBox hint2;
    private final JCheckBox hint3;

    private final JTextField resultField;
    private final JLabel hintLabel;
    private final JComboBox topics;

    private final JButton checkButton;
    private final JButton showResultButton;
    private final JButton skipButton;

    public Controlls() {
        wordLabel = new JLabel();
        hint1 = new JCheckBox();
        hint2 = new JCheckBox();
        hint3 = new JCheckBox();
        resultField =  new JTextField();
        topics = new JComboBox();
        hintLabel = new JLabel();
        checkButton = new JButton();
        showResultButton = new JButton();
        skipButton = new JButton();
    }
    public JLabel getWordLabel() {
        return wordLabel;
    }

    public JCheckBox getHint1() {
        return hint1;
    }

    public JCheckBox getHint2() {
        return hint2;
    }

    public JCheckBox getHint3() {
        return hint3;
    }


    public JTextField getResultField() {
        return resultField;
    }

    public JLabel getHintLabel() {
        return hintLabel;
    }

    public JComboBox getTopics() {
        return topics;
    }

    public JButton getCheckButton() {
        return checkButton;
    }

    public JButton getShowResultButton() {
        return showResultButton;
    }

    public JButton getSkipButton() {
        return skipButton;
    }

    public void setEnableAllButtons(boolean isEnabled) {
        checkButton.setEnabled(isEnabled);
        showResultButton.setEnabled(isEnabled);
        skipButton.setEnabled(isEnabled);
    }


}
