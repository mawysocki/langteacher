package layout;

public interface Initializer {

    void initComponent();
    void setFontComponent();
    void addCompoment();

    void disableComponent();
}
