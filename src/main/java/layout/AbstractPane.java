package layout;

import javax.swing.*;
import java.awt.*;
import java.util.jar.JarEntry;

public abstract class AbstractPane extends JPanel implements Initializer {
    protected Controlls controlls;
    protected final Font font = new Font("SansSerif", Font.BOLD, 30);
    public AbstractPane(Controlls controlls) {
        this.controlls = controlls;
        this.setLayout(new GridLayout(9, 1));
        setFontComponent();
        initComponent();
        addCompoment();
        disableComponent();
    }
}
