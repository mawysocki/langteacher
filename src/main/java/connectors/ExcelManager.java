package connectors;

import logic.Logic;
import logic.Points;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;

import java.util.ArrayList;
import java.util.List;

public class ExcelManager {


    private List<String> toTranslate;
    private List<String> translation;
    private List<String> articles;
    private final XSSFSheet sheet;

    public ExcelManager(String topic, int part, boolean isGender) {
        ExcelConnector connector = new ExcelConnector(topic);
        sheet = connector.getSheet(part);
        createLists(isGender);
        connector.close();
    }

    private void createLists(Boolean isGender) {
        toTranslate = new ArrayList<>();
        translation = new ArrayList<>();
        if (isGender) {
            articles = new ArrayList<>();
        }

        for (Row row : sheet) {
            toTranslate.add(row.getCell(0).getStringCellValue());
            translation.add(row.getCell(1).getStringCellValue());
            if (isGender) {
                articles.add(row.getCell(2).getStringCellValue());
            }
        }
        Points.calculateMaxPoints(translation);
        Points.resetPoints();
        if (isGender) {
            Logic.getInstance().createDicts(toTranslate, translation, articles);
        }
        else {
            Logic.getInstance().createDicts(toTranslate, translation);
        }
    }

    public void displayAll() {
        toTranslate.forEach(System.out::println);
    }
}
